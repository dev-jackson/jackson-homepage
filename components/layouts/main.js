import Head from 'next/head'
import Navbar from '../navbar.js'
import { Box, Container } from '@chakra-ui/react'

const Main = ({ children, router }) => {
  return (
    <Box as="main" pb={6}>
      <Head>
        <link rel="icon" href="/images/head.ico" />
        <meta name="viewport" content="width=devices-width, initial-scale=1" />
        <title>Jackson Sanchez - Homepage</title>
      </Head>
      <Navbar path={router.asPath} />
      <Container maxW="container.md" pt={16}>
        {children}
      </Container>
    </Box >
  )
}

export default Main
