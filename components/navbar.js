import Logo from './logo'
import NextLink from 'next/link'
import {
  Container,
  Box,
  Link,
  Stack,
  Heading,
  Flex,
  Menu,
  MenuItem,
  MenuList,
  MenuButton,
  IconButton
} from '@chakra-ui/react'
import { HamburgerIcon } from '@chakra-ui/icons'
import { useColorModeValue } from '@chakra-ui/react'
import ThemeToggleButton from './theme-toggle-button'
import {
  IoLogoGithub,
  IoLogoGitlab
} from 'react-icons/io5'

const LinkItem = ({ href, path, children }) => {
  const active = path === href
  const inactivColor = useColorModeValue('gray200', 'whiteAlpha.900')

  function toggleHover(e) {
    if (e.type == 'mouseover') {
      e.target.style.fontWeight = 600;
    } else {
      e.target.style.fontWeight = 100;
    }

  }
  return (
    <NextLink href={href}>
      <Link
        p={2}
        borderRadius={5}
        onMouseOver={toggleHover}
        onMouseLeave={toggleHover}
        style={{ 
          textDecoration: 'none', 
          transition: 'all .25s'
        }}
        bg={active ? '#3d7aed' : undefined}
        color={active ? "#202023" : inactivColor}>
        {children}
      </Link>
    </NextLink>
  )
}

const Navbar = props => {
  const { path } = props

  return (
    <Box
      position="fixed"
      as="nav"
      w="100%"
      bg={useColorModeValue('#fffff40', '#20202330')}
      style={{ backdropFilter: 'blur(8px)' }}
      zIndex={1}>
      <Container
        display="flex"
        p={2}
        maxW="container.md"
        wrap="wrap"
        align="center"
        justify="space-between">
        <Flex align="center" mr={6}>
          <Heading as='h1' size="lg" letterSpacing={'tight'}>
            <Logo />
          </Heading>
        </Flex>
        <Stack
          direction={{ base: 'column', md: 'row' }}
          display={{ base: 'none', md: 'flex' }}
          width={{ base: 'full', md: 'auto' }}
          alignItems="center"
          flexGrow={1}
          mt={{ base: 4, md: 0 }}>
          <LinkItem href="/works" path={path}>
            Work
          </LinkItem>
          <LinkItem href="/posts">
            Post
          </LinkItem>
          <LinkItem href="/tools">
            Tools
          </LinkItem>
          <Link href="https://github.com/dev-jackson" target="_blank">
            <IoLogoGithub size={26} color={useColorModeValue('#000000', '#FFFFFF')} />
          </Link>
          <Link href="https://gitlab.com/dev-jackson" target="_blank">
            <IoLogoGitlab size={26} color={useColorModeValue('#000000', '#FFFFFF')} />
          </Link>
        </Stack>
        <Box flex={1} align="right">
          <ThemeToggleButton />
          <Box ml={2} display={{ base: 'inline-block', md: 'none' }}>
            <Menu>
              <MenuButton as={IconButton} icon={<HamburgerIcon />} variant="outline" aria-label="Options" />
              <MenuList>
                <NextLink href="/" passHref >
                  <MenuItem as={Link}>About</MenuItem>
                </NextLink>
                <NextLink href="/works" passHref>
                  <MenuItem as={Link}>Works</MenuItem>
                </NextLink>
                <NextLink href="/post" passHref>
                  <MenuItem as={Link}>Post</MenuItem>
                </NextLink>
                <NextLink href="/tools" passHref>
                  <MenuItem as={Link}>Tools</MenuItem>
                </NextLink>
                <MenuItem align="center" alignContent="center" alignSelf="center" >
                  <Link href="https://github.com/dev-jackson" target="_blank" p={4}> 
                    <IoLogoGithub size={26} color={useColorModeValue('#000000', '#FFFFFF')} />
                  </Link>
                  <Link href="https://gitlab.com/dev-jackson" target="_blank" p={4}>
                    <IoLogoGitlab size={26} color={useColorModeValue('#000000', '#FFFFFF')} />
                  </Link>
                </MenuItem>
              </MenuList>
            </Menu>
          </Box>
        </Box>
      </Container>
    </Box>
  )
}

export default Navbar
