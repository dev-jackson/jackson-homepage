import { Container, Heading, SimpleGrid } from '@chakra-ui/react'
import Layout from '../components/layouts/article'
import Section from '../components/section'
import {
  GridItem
} from '../components/grid-item'
import thumbVsCode from '../public/images/visual-studio-code.jpeg'
import thumbLvim from '../public/images/lvim.png'
import thumbZeal from '../public/images/zeal.png'
import thumbOs from '../public/images/os.png'
import thumbGitHub from '../public/images/github.png'

const Tools = () => {
  return (
    <Layout title="Tools">
      <Container>
        <Heading as="h3" fontSize={20} mb={4}>
          My Tools
        </Heading>
        <Section dealy={0.1}>
          <Heading as="h4" fontSize={18} p={2} align="center">
            OS
          </Heading>
          <SimpleGrid>
            <GridItem
              title="ArchLinux"
              thumbnail={thumbOs}
              href="https://archlinux.org/">
            </GridItem>
          </SimpleGrid>
        </Section>
        <Section dealy={0.1}>
          <Heading as="h4" fontSize={18} p={2} align="center">
            Editors
          </Heading>
          <SimpleGrid column={[1, 2, 3]} gap={6}>
            <GridItem
              title="My favorite use editor"
              thumbnail={thumbLvim}
              href="https://github.com/LunarVim/LunarVim" />
            <GridItem
              title="My second use editor"
              thumbnail={thumbVsCode}
              href="https://code.visualstudio.com/" />
          </SimpleGrid>
        </Section>
        <Section>
          <Heading ad="h3" fontSize={18} align="center" p={2}>
            Documentation
          </Heading>
          <SimpleGrid>
            <GridItem
              title="Zeal"
              thumbnail={thumbZeal}
              href="https://zealdocs.org/">
            </GridItem>
          </SimpleGrid>
        </Section>
        <Section>
          <Heading ad="h3" fontSize={18} align="center" p={2}>
            Versioning platform
          </Heading>
          <SimpleGrid>
            <GridItem
              title="GitHub"
              thumbnail={thumbGitHub}
              href="https://github.com/">
            </GridItem>
          </SimpleGrid>
        </Section>
      </Container>
    </Layout>
  )
}

export default Tools
