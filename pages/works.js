import { Container, Heading, SimpleGrid } from '@chakra-ui/react'
import { GridItem } from '../components/grid-item'
import thumbFubLige from '../public/images/fublige.png'
//import Section from '../components/section'
//import { WorkGridItem } from '../components/grid-item'

const Works = () => {
  return (
    <Container>
      <Heading as="h3" fontSize={20} mb={4}>
        Works
      </Heading>
      <SimpleGrid columns={[1]} gap={6} >
        <GridItem
          title="FubLige desing(alpha)"
          thumbnail={thumbFubLige}
          href="https://www.figma.com/file/H9SYsPVJ6zaPjrCzGOhjcC/Untitled?node-id=1%3A3">
        </GridItem>
      </SimpleGrid>
    </Container>
  )
}

export default Works
