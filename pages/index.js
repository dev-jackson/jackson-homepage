import {
  Container,
  Box,
  Heading,
  // Image,
  List,
  ListItem,
  Icon,
  Link,
  Button,
  Avatar
} from '@chakra-ui/react'
import Section from '../components/section.js'
import Paragraph from '../components/paragraph'
import { ChevronRightIcon } from '@chakra-ui/icons'
import NextLink from 'next/link'
import { BioSection, BioYear } from '../components/bio'
import Layout from '../components/layouts/article'
//import {GridItem} from '../components/bio'
import {
  IoLogoTwitter,
  IoLogoInstagram,
} from 'react-icons/io5'

const Page = () => {
  return (
    <Layout>
      <Container>
        {/* <Box borderRadius="lg" */}
        {/*   bg={useColorModeValue('#EAEDED', '#979A9A')} */}
        {/*   p={3} */}
        {/*   mb={6} */}
        {/*   align="center"> */}
        {/*   <div>Hello, I&apos;m a full-sctack base in Ecuador</div > */}
        {/* </Box> */}
        <Box display={{ md: 'flex' }}>
          <Box flexGrow={1}>
            <Box flexShrink={0} mt={{ base: 4, md: 0 }} ml={{ md: 8 }} align="center" padding={6}>
              <Avatar src="/images/profile-image.jpg"
                sx={{ width: 150, height: 150 }}></Avatar>
              {/* <Image borderColor="whiteAlpha.800" */}
              {/*   borderWidth={2} */}
              {/*   borderStyle="solid" */}
              {/*   maxWidth="160px" */}
              {/*   display="inline-block" */}
              {/*   borderRadius="full" */}
              {/*   src="/images/profile-image.jpg" */}
              {/*   alt="Profile Image" /> */}
            </Box>
            <Heading as="h2" variant="page-title">
              Jackson Sanchez
            </Heading>
            <p>Hi, I prograameDigital (Artist / Developer / Designer).</p>
            <p>In a world where technology dominates everything, tools can be created to help everyone.</p>
          </Box>
        </Box>
        <Section dealy={0.1}>
          <Heading as="h3" variant="section-title">
            Work
          </Heading>
          <Paragraph>Paragraph</Paragraph>
          <Box align="center" my={4}>
            <NextLink href="/works">
              <Button rightIcon={<ChevronRightIcon />} colorScheme="blue">
                My portfolio
              </Button>
            </NextLink>
          </Box>
        </Section>
        <Section dealy={0.2}>
          <Heading as="h3" variant="section-title">
            Bio
          </Heading>
          <BioSection>
            <BioYear>1998</BioYear>
            Born in Ventanas, Ecuador.
          </BioSection>
          <BioSection>
            <BioYear>2022</BioYear>
            Completed the Computerstystems engineer in the
            Guayaquil University.
          </BioSection>
        </Section>
        <Section dealy={0.3}>
          <Heading as="h3" variant="section-title">
            On the Web
          </Heading>
          <List>
            {/* <ListItem> */}
            {/*   <Link href="https://github.com/dev-jackson" target="_blank"> */}
            {/*     <Button variant="ghost" colorScheme="teal" leftIcon={<Icon as={IoLogoGithub} />}> */}
            {/*       @dev-jackson */}
            {/*     </Button> */}
            {/*   </Link> */}
            {/* </ListItem> */}
            <ListItem>
              <Link href="https://instagram.com/dev-jackson" target="_blank">
                <Button variant="ghost" colorScheme="blue" leftIcon={<Icon as={IoLogoInstagram} />}>
                  @JacksonASR
                </Button>
              </Link>
            </ListItem>
            <ListItem>
              <Link href="https://instagram.com/dev-jackson" target="_blank">
                <Button variant="ghost" colorScheme="blue" leftIcon={<Icon as={IoLogoTwitter} />}>
                  @JacksonASR
                </Button>
              </Link>
            </ListItem>
            {/* <ListItem> */}
            {/*   <Link href="https://gitlab.com/dev-jackson" target="_blank"> */}
            {/*     <Button variant="ghost" colorScheme="blue" leftIcon={<Icon as={IoLogoGitlab} />}> */}
            {/*       @dev-jackson */}
            {/*     </Button> */}
            {/*   </Link> */}
            {/* </ListItem> */}
          </List>
        </Section>
      </Container >
    </Layout>
  )
}
export default Page
